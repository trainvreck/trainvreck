﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class Generationtest
    {
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        public IEnumerator DestroysEnemies()
        {
            var prefab = Resources.Load("Prefabs/hill and a shed 1");
            var objects = GameObject.FindGameObjectsWithTag("Ground");
            var temp = Resources.Load("Scripts/ThredmillGeneration");

            int objectCountBeforeRemove = objects.Length;

            var train = new GameObject();
            var generator = train.AddComponent<ThredmillController>();
            generator.essentialPrefabGround = (GameObject)prefab;
            generator.essentialPrefabTracks = (GameObject)prefab;
            var prefabs = new GameObject[1];
            prefabs[0] = (GameObject)prefab;
            generator.prefabs = prefabs;

            yield return null;

            train.transform.position += new Vector3(100, 0, 0);

            yield return null;

            objects = GameObject.FindGameObjectsWithTag("Ground");
            int objectCountAfterRemove = objects.Length;

            //Assert.AreEqual(objectCountAfterRemove, objectCountBeforeRemove);

        }
    }
}
