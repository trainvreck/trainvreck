﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class changingParts : MonoBehaviour
{
    public GameObject train;
    private bool Armor0 = false;
    private bool Armor1 = false;
    private bool wheel_Armor = false;
    private bool wheel_Armor1 = false;
    private bool boiler_Armor = false;
    private bool Ram = false;
    private bool Weight = false;
    private bool Smokestack = false;
    Transform A0, A1, WA0, WA1, BA, RA, WH, SM0, SM1;

    private void Start()
    {
       A0 = train.transform.Find("Armor01");
       A1 = train.transform.Find("Armor2");
       WA0 = train.transform.Find("Wheel Armor");
        WA1 = train.transform.Find("Wheel Armor1");
        BA = train.transform.Find("Cage");
        RA = train.transform.Find("Ram");
        WH = train.transform.Find("Weight");
        SM0 = train.transform.Find("Base/Smokestack");
        SM1 = train.transform.Find("Base/Smokestack1");
    }

    void OnGUI()
    {

        Armor0 = GUI.Toggle(new Rect(10, 10, 100, 20), Armor0, "Front Armor");
        Armor1 = GUI.Toggle(new Rect(10, 35, 100, 20), Armor1, "Side Armor");
        wheel_Armor = GUI.Toggle(new Rect(10, 60, 100, 20), wheel_Armor, "Back Wheel armor");
        wheel_Armor1 = GUI.Toggle(new Rect(10, 85, 100, 20), wheel_Armor1, "Front Wheel armor");
        boiler_Armor = GUI.Toggle(new Rect(10, 110, 100, 20), boiler_Armor, "Boiler armor");
        Ram = GUI.Toggle(new Rect(10, 135, 100, 20), Ram, "Ram");
        Weight = GUI.Toggle(new Rect(10, 160, 100, 20), Weight, "Weight");
        Smokestack = GUI.Toggle(new Rect(10, 185, 100, 20), Smokestack, "Smokestack");

    }

    private void Update()
    {
        A0.gameObject.SetActive(Armor0);
        A1.gameObject.SetActive(Armor1);
        WA0.gameObject.SetActive(wheel_Armor);
        WA1.gameObject.SetActive(wheel_Armor1);
        BA.gameObject.SetActive(boiler_Armor);
        RA.gameObject.SetActive(Ram);
        WH.gameObject.SetActive(Weight);
        SM0.gameObject.SetActive(!Smokestack);
        SM1.gameObject.SetActive(Smokestack);

    }

  

}
