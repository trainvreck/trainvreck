﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace customScripts
{
    public class CoalControler : MonoBehaviour
    {

        public GameObject train;
        public GameObject coalSpawnPoint;
        public GameObject coalPrefab;

        public int startingCoalAmmount = 5;
        public float coalForce = 5f;
        public float forceDecayAmmount = 5f;

        public float coalBurnTime = 5f;
        public float forceDecayTime = 5f;


        private List<GameObject> coalInFurnace = new List<GameObject>();
        private ThredmillController thredmillController;
        private float coalBurnTimer;
        private float forceDecayTimer;
        // Start is called before the first frame update
        void Start()
        {
            thredmillController = train.GetComponent<ThredmillController>();
        }

        // Update is called once per frame
        void Update()
        {
            if (coalBurnTimer >= coalBurnTime)
            {
                BurnCoal();
                coalBurnTimer = 0;
            }
            if (forceDecayTimer >= forceDecayTime)
            {
                SubtractForce();
                forceDecayTimer = 0;
            }


            forceDecayTimer += Time.deltaTime;
            coalBurnTimer += Time.deltaTime;
        }

        private void OnTriggerEnter(Collider col)
        {
            if(col.gameObject.tag == "Coal")
            {
                AddForce();
                coalInFurnace.Add(col.gameObject);
            }

        }

        private void BurnCoal()
        {
            if (coalInFurnace.Count != 0)
            {
                coalInFurnace[0].transform.position = coalSpawnPoint.transform.position;
                coalInFurnace.RemoveAt(0);
            }
        }

        private void SubtractForce()
        {
            thredmillController.ChangeMaxForce(-forceDecayAmmount);
        }
        private void AddForce()
        {
            thredmillController.ChangeMaxForce(coalForce);
        }
    }
}
