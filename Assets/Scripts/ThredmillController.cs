﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ThredmillController : MonoBehaviour
{
    public bool brake = false;
    //All prefabs must be the same size in X and Y axis
    [Header("Prefabs for randomisation")]
    public GameObject[] prefabs;
    [Header("Prefab for tracks")]
    public GameObject essentialPrefabTracks;
    public GameObject essentialPrefabGround;
    public float trackGroundHeigthDiff = -1.15f;
    public float trainTrackDifference = -2.1f;
    public float prefabXDim = 50;
    public int frontSections = 10;

    internal int CalculateScoreByDistance(int timerTickSize)
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>();
        Vector3 posibleSpeed = new Vector3(-1, 0, 0) * maxSpeed;
        Vector3 currSpeed = rigidBody.velocity;
        float procSpeed = currSpeed.x / posibleSpeed.x;
        int score = Convert.ToInt32(Math.Floor(procSpeed * 100));
        return score;
    }

    public int behindSections = 3;
    public int sideSectionCount = 3;

    //Environment force parameters
    [Header("Movement parameters")]
    public int maxSpeed;
    private int speed;
    private float maxForce;
    private float forceMultiplyer;
    [Header("Breaking parameters")]
    public float maxBreakingForce;
    public float breakingForceMultiplyer;

    //temp
    //public int SPEED;
    public float MAXFORCE = 100;
    public float FORCEMULT = 10;

    private float prevX;
    private System.Random rnd = new System.Random();

    //Event handlers for environment control
    private EventHandler<int> speedChanged;
    private EventHandler<float> maxForceChanged;
    private EventHandler<float> forceMultiplyerChanged;

    public LeverController speedWheel;

    private List<GameObject> queue;
    void Start()
    {
        //maxForce = MAXFORCE;
        forceMultiplyer = FORCEMULT;
        prevX = transform.position.x;
        queue = new List<GameObject>();
        InstantiateInitialPrefabs();
        speedWheel.onValueChanged.AddListener(ChangeSpeed);
    }

    private void OnDestroy()
    {
        speedWheel.onValueChanged.RemoveListener(ChangeSpeed);
    }


    void Update()
    {
        Transform prefabTransform = queue[queue.Count - 1].GetComponent<Transform>();
        if (prefabTransform.position.x <= transform.position.x + (frontSections - 2) * prefabXDim)
        {
            InstantiateDestroyPrefab(prevX, transform.position.y + trainTrackDifference, transform.position.z, essentialPrefabTracks, queue);
            InstantiateDestroyPrefab(prevX, transform.position.y + trackGroundHeigthDiff + trainTrackDifference, 0, essentialPrefabGround, queue);

            for (int i = 1; i <= sideSectionCount; i++)
            {
                InstantiateDestroyPrefab(prevX, transform.position.y + trackGroundHeigthDiff + trainTrackDifference, prefabXDim * i, RandomObject(rnd, prefabs), queue);
                InstantiateDestroyPrefab(prevX, transform.position.y + trackGroundHeigthDiff + trainTrackDifference, -prefabXDim * i, RandomObject(rnd, prefabs), queue);
            }
        }
    }
    /// <summary>
    /// Renders initial rail and ground prefabs
    /// </summary>
    void InstantiateInitialPrefabs()
    {
        float xTemp = transform.position.x;
        float yTemp = transform.position.y + trainTrackDifference;
        for (int i = -behindSections; i < frontSections; i++)
        {
            //Add tracks and ground
            InstantiatePrefab(xTemp + prefabXDim * i, yTemp, transform.position.z, essentialPrefabTracks, queue);
            InstantiatePrefab(xTemp + prefabXDim * i, yTemp + trackGroundHeigthDiff, transform.position.z, essentialPrefabGround, queue);
            for (int j = 1; j <= sideSectionCount; j++)
            {
                //Add side tiles
                InstantiatePrefab(xTemp + prefabXDim * i, yTemp + trackGroundHeigthDiff, prefabXDim * j, RandomObject(rnd, prefabs), queue);
                InstantiatePrefab(xTemp + prefabXDim * i, yTemp + trackGroundHeigthDiff, -prefabXDim * j, RandomObject(rnd, prefabs), queue);
            }
        }
        prevX = transform.position.x + prefabXDim * (frontSections - 1);
    }

    void InstantiateDestroyPrefab(float x, float y, float z, GameObject prefab, List<GameObject> queue)
    {
        GameObject gameObject = Instantiate(prefab, new Vector3(x, y, z), Quaternion.identity);
        gameObject.AddComponent<TileMovementScript>();
        TileMovementScript script = gameObject.GetComponent<TileMovementScript>();
        script.SetParams(speed, maxForce, forceMultiplyer);
        script.Subscribe(ref this.speedChanged, ref this.maxForceChanged, ref this.forceMultiplyerChanged);
        queue.Add(gameObject);

        Destroy(queue[0]);
        queue.RemoveAt(0);
    }
    void InstantiatePrefab(float x, float y, float z, GameObject prefab, List<GameObject> queue)
    {
        GameObject gameObject = Instantiate(prefab, new Vector3(x, y, z), Quaternion.identity);
        gameObject.AddComponent<TileMovementScript>();
        TileMovementScript script = gameObject.GetComponent<TileMovementScript>();
        script.SetParams(speed, maxForce, forceMultiplyer);
        script.Subscribe(ref this.speedChanged, ref this.maxForceChanged, ref this.forceMultiplyerChanged);
        queue.Add(gameObject);
    }
    GameObject RandomObject(System.Random rnd, GameObject[] prefabs)
    {
        return prefabs[rnd.Next(prefabs.Length)];
    }

    public void ChangeSpeed(int newSpeed)
    {
        if (newSpeed < 0)
        {
            speed = 0;
        }
        else if (newSpeed > maxSpeed)
        {
            speed = maxSpeed;
        }
        else
        {
            speed = newSpeed;
        }

        speedChanged?.Invoke(this, speed);
    }

    public void ChangeSpeed(float angle)
    {
        ChangeSpeed((int)(((angle + 180)/360)*maxSpeed));
    }

    public void ChangeMaxForce(float changeAmount)
    {
        if (maxForce + changeAmount < 0)
        {
            maxForce = 0;
        }
        else
        {
            maxForce += changeAmount;
        }
        maxForceChanged?.Invoke(this, maxForce);
    }

    public void ChangeForceMultiplyer(float changeAmount)
    {
        if (forceMultiplyer + changeAmount < 0)
        {
            forceMultiplyer = 0;
        }
        else
        {
            forceMultiplyer += changeAmount;
        }
        forceMultiplyerChanged?.Invoke(this, forceMultiplyer);
    }

    public void Brake()
    {
        speedChanged(this, 0);
        maxForceChanged(this, maxBreakingForce);
        forceMultiplyerChanged(this, breakingForceMultiplyer);
    }
    public void ContinueMoving()
    {
        speedChanged(this, speed);
        maxForceChanged(this, maxForce);
        forceMultiplyerChanged(this, forceMultiplyer);
    }
}
