﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonController : MonoBehaviour
{
    public UnityEvent buttonPress;
    public UnityEvent onButtonPress;
    public UnityEvent onButtonUnpress;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Button"))
        {
            onButtonPress.Invoke();
            Debug.Log("Button pressed");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Button"))
        {
            onButtonUnpress.Invoke();
            Debug.Log("Button unpressed");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Button"))
        {
            buttonPress.Invoke();
            Debug.Log("Button is pressed");
        }
    }
}
