﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(HingeJoint))]
public class LeverController : MonoBehaviour
{
    private HingeJoint joint;

    public float triggerAngle;
    public float angleStep;
    private float oldAngle;
    private bool entered;

    public UnityEvent leverEnter;
    public UnityEvent onLeverEnter;
    public UnityEvent onLeverLeave;
    public UnityFloatEvent onValueChanged;

    public bool debugAngle = false;

    void Start()
    {
        joint = GetComponent<HingeJoint>();
        oldAngle = joint.angle;
    }

    void FixedUpdate()
    {
        HandleValueChanged();
        HandleEnterLeave();
    }

    private void HandleEnterLeave()
    {
        if (oldAngle >= triggerAngle)
        {
            if (!entered)
            {
                entered = true;
                onLeverEnter.Invoke();
                Debug.Log("lever entered");
            }
            leverEnter.Invoke();
        }
        else if (oldAngle < triggerAngle)
        {
            if (entered)
            {
                entered = false;
                onLeverLeave.Invoke();
                Debug.Log("lever leave");
            }
        }
    }

    private void HandleValueChanged()
    {
        if (debugAngle)
        {
            Debug.Log($"stepped angle: {oldAngle} | real angle: {joint.angle}");
        }

        if (oldAngle + angleStep < joint.angle)
        {
            oldAngle += angleStep;
            onValueChanged.Invoke(oldAngle);
        }

        if (oldAngle - angleStep > joint.angle)
        {
            oldAngle -= angleStep;
            onValueChanged.Invoke(oldAngle);
        }

       

    }
}

[System.Serializable]
public class UnityFloatEvent : UnityEvent<float>
{

}
