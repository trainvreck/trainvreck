﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour
{
    [HideInInspector]
    public Hand activeHand = null;

    public Vector3 offset = Vector3.zero;
    public bool useOffset = false;

    public void Awake()
    {
        Collider col = GetComponent<Collider>();
        if (col == null)
        {
            MeshCollider meshCol = gameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
            meshCol.convex = true;
        }
        else
        {
            col.enabled = true;
        }
    }

    public virtual void Action()
    {
        Debug.Log("Interactable action invoked.");
    }

    public void ApplyOffset(Transform hand)
    {
        if (useOffset)
        {
            transform.SetParent(hand);
            transform.localRotation = Quaternion.identity;
            transform.localPosition = offset;
            transform.SetParent(null);
        }
    }
}
