﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwitchController : MonoBehaviour
{
    private HingeJoint joint;

    private readonly float angleStep = 1;
    private float oldAngle;
    private bool entered;
    private bool springSet;

    private JointSpring spring;
    private JointLimits limits;

    public float triggerAngle;
    public bool startOn;

    public UnityEvent switchOn;
    public UnityEvent switchOff;

    void Start()
    {
        joint = GetComponent<HingeJoint>();

        spring.spring = 80;
        spring.damper = 25;
        if (startOn)
        {
            spring.targetPosition = triggerAngle;
            springSet = true;
        }
        else
        {
            spring.targetPosition = -triggerAngle;
            springSet = false;
        }

        joint.spring = spring;

        limits.min = -triggerAngle;
        limits.max = triggerAngle;
        limits.bounciness = 1;
        limits.bounceMinVelocity = 0.2f;
        limits.contactDistance = 0;

        joint.limits = limits;
        
        oldAngle = joint.angle;
    }

    void Update()
    {
        HandleValueChanged();
        HandleSpring();
        HandleOnOff();
    }    

    private void HandleOnOff()
    {
        if (oldAngle >= triggerAngle)
        {
            if (!entered)
            {
                entered = true;
                switchOn.Invoke();
                Debug.Log("switch on");
            }
        }
        else if (oldAngle <= triggerAngle)
        {
            if (entered)
            {
                entered = false;
                switchOff.Invoke();
                Debug.Log("switch off");
            }
        }
    }

    private void HandleSpring()
    {
        if (oldAngle > 0)
        {
            if (!springSet)
            {
                spring.targetPosition = triggerAngle;
                joint.spring = spring;
                springSet = true;
            }
        }
        else if (oldAngle < 0)
        {
            if (springSet)
            {
                spring.targetPosition = -triggerAngle;
                joint.spring = spring;
                springSet = false;
            }
        }
    }

    private void HandleValueChanged()
    {
        if (oldAngle + angleStep < joint.angle)
        {
            oldAngle += angleStep;
        }

        if (oldAngle - angleStep > joint.angle)
        {
            oldAngle -= angleStep;
        }
    }
}