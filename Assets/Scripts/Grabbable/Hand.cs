﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(SphereCollider), typeof(SteamVR_Behaviour_Pose), typeof(FixedJoint))]
public class Hand : MonoBehaviour
{
    public SteamVR_Action_Boolean grabAction;
    public SteamVR_Action_Boolean interactableAction;

    private SteamVR_Behaviour_Pose pose;
    private FixedJoint joint;
    public SphereCollider sphereCollider;

    private Grabbable currentInteractable;
    private List<Grabbable> contactInteractables = new List<Grabbable>();


    void Awake()
    {
        pose = GetComponent<SteamVR_Behaviour_Pose>();
        joint = GetComponent<FixedJoint>();
    }

    void Update()
    {
        if (grabAction.GetStateDown(pose.inputSource))
        {
            Pickup();
        }

        if (grabAction.GetStateUp(pose.inputSource))
        {
            Drop();
        }

        if (interactableAction.GetStateDown(pose.inputSource))
        {
            if (currentInteractable != null)
            {
                currentInteractable.Action();
            }
        }
    }

    public void Pickup()
    {
        currentInteractable = GetNearestInteractable();

        if (currentInteractable == null)
        {
            return;
        }

        if (currentInteractable.activeHand != null)
        {
            currentInteractable.activeHand.Drop();
        }
        sphereCollider.enabled = false;
        currentInteractable.ApplyOffset(transform);
        joint.connectedBody = currentInteractable.GetComponent<Rigidbody>();
        currentInteractable.activeHand = this;
    }

    public void Drop()
    {
        if (currentInteractable == null)
        {
            return;
        }

        Rigidbody interactableBody = currentInteractable.GetComponent<Rigidbody>();
        Vector3 velocity = pose.GetVelocity();
        interactableBody.velocity = new Vector3(velocity.x, velocity.y, velocity.z);
        interactableBody.angularVelocity = pose.GetAngularVelocity();

        joint.connectedBody = null;
        currentInteractable.activeHand = null;
        currentInteractable = null;
        StartCoroutine(LateColliderActivate());
    }

    private IEnumerator LateColliderActivate()
    {
        yield return new WaitForSeconds(0.5f);
        sphereCollider.enabled = true;
    }

    public Grabbable GetNearestInteractable()
    {
        Grabbable nearest = null;
        float min = float.MaxValue;

        foreach (Grabbable interactable in contactInteractables)
        {
            float distance = (interactable.transform.position - transform.position).sqrMagnitude;
            if (distance < min)
            {
                nearest = interactable;
                min = distance;
            }
        }

        return nearest;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Grabbable>() != null)
        {
            contactInteractables.Add(other.gameObject.GetComponent<Grabbable>());
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Grabbable>() != null) //possible performance improvement to check tag rather than to check component
        {
            contactInteractables.Remove(other.gameObject.GetComponent<Grabbable>());
        }
    }
}
