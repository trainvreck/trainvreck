﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TileMovementScript : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody rigidBody;
    private Vector3 direction = new Vector3(-1,0,0);
    private int speed = 0;
    private float maxForce = 0;
    private float forceMult = 0;

    void Start()
    {
        rigidBody = gameObject.AddComponent<Rigidbody>();
        rigidBody.isKinematic = false;
        rigidBody.useGravity = false;
        rigidBody.mass = 1;
        rigidBody.drag = 1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 targetVelocity = direction * speed;
        Vector3 force = (targetVelocity - rigidBody.velocity) * forceMult;
        if (force.magnitude > maxForce)
        {
            force = force.normalized * maxForce;
        }
        rigidBody.AddForce(force);
    }
    public void SetParams(int speed, float maxForce, float forceMult)
    {
        this.speed = speed;
        this.maxForce = maxForce;
        this.forceMult = forceMult;
    }
    private void OnSpeedChanged(object sender, int speed)
    {
        this.speed = speed;
    }
    private void OnMaxForceChanged(object sender, float maxForce)
    {
        this.maxForce = maxForce;
    }
    private void OnForceMultiplyerChanged(object sender, float forceMult)
    {
        this.forceMult = forceMult;
    }
    public void Subscribe(ref EventHandler<int> speedChanged, ref EventHandler<float> maxForceChanged, ref EventHandler<float> forceMultChanged)
    {
        speedChanged += OnSpeedChanged;
        maxForceChanged += OnMaxForceChanged;
        forceMultChanged += OnForceMultiplyerChanged;
    }
}
