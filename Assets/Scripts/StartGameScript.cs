﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameScript : MonoBehaviour
{
    public void OnButtonPress()
    {
        FindObjectOfType<LevelManager>().LoadGame();
    }
}
