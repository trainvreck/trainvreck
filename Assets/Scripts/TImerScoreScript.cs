﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TImerScoreScript : MonoBehaviour
{
    public ThredmillController controller;
    public int timeInSeconds;
    public int score;
    public int timeLeft;

    void Start()
    {
        StartTimerForGame(1);
    }

    public void StartTimerForGame(int timerTickSize)
    {
       
        StartCoroutine(TimerCoroutine(
            () =>
            {
                FindObjectOfType<LevelManager>().LoadIntro(2f);
            },
            () =>
            {
                if (controller != null)
                {
                    score += controller.CalculateScoreByDistance(timerTickSize);
                }
            }, timerTickSize));
        ;
    }

    private IEnumerator TimerCoroutine(Action timerEnd = null, Action timerTickEvent = null, int timerTickSize = 1)
    {
        timeLeft = timeInSeconds;
        for (int i = timeInSeconds; i > 0; i -= timerTickSize)
        {
            yield return new WaitForSeconds(timerTickSize);
            timeLeft -= timerTickSize;
            if (timerTickEvent != null)
            {
                timerTickEvent.Invoke();
            }
        }

        if (timerEnd != null)
        {
            timerEnd.Invoke();
        }
    }
}
