﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRLocomotion : MonoBehaviour
{

    public float sensitivity = 0.1f;
    public float maxSpeed = 1f;

    public SteamVR_Action_Boolean touchpadPress;
    public SteamVR_Action_Vector2 moveValue;

    public SteamVR_Behaviour_Pose rightController, leftController;

    private Vector3 speed;
    private CharacterController characterController;
    private Transform cameraRig;
    private Transform head;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Start()
    {
        cameraRig = SteamVR_Render.Top().origin;
        head = SteamVR_Render.Top().head;
    }

    void Update()
    {
        HandleHead();
        HandleCenter();
        CalculateMovement();
    }

    public void HandleHead()
    {
        Vector3 oldPos = cameraRig.position;
        Quaternion oldRot = cameraRig.rotation;

        transform.eulerAngles = new Vector3(0f, head.rotation.eulerAngles.y, 0f);

        cameraRig.position = oldPos;
        cameraRig.rotation = oldRot;
    }

    private void CalculateMovement()
    {
        Vector3 orientationEuler = new Vector3(0, transform.eulerAngles.y, 0);
        Quaternion orientation = Quaternion.Euler(orientationEuler);
        Vector3 movement = Vector3.zero;

        if (touchpadPress.GetStateUp(leftController.inputSource))
        {
            speed = Vector2.zero;
        }

        if (touchpadPress.GetState(leftController.inputSource))
        {
            speed += new Vector3(moveValue.axis.x,0 ,moveValue.axis.y);
            speed = Vector3.ClampMagnitude(speed, maxSpeed);

            movement += orientation * speed;
        }
        //Debug.Log(movement);
        characterController.SimpleMove(movement);
    }

    private void HandleCenter()
    {
        float headHeight = Mathf.Clamp(head.position.y, 1, 2);
        characterController.height = headHeight;

        Vector3 newCenter = Vector3.zero;
        newCenter.y = characterController.height / 2;
        newCenter.y += characterController.skinWidth;
        newCenter.x = head.localPosition.x;
        newCenter.z = head.localPosition.z;

        newCenter = Quaternion.Euler(0, -transform.eulerAngles.y, 0) * newCenter;

        characterController.center = newCenter;

    }
}
