﻿using UnityEngine;

namespace customScripts
{
    public class SimpleMoveObjectScript : MonoBehaviour
    {
        public float speed = 1f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            Vector3 pos = new Vector3(transform.position.x + Input.GetAxis("Vertical") * speed, transform.position.y, transform.position.z + Input.GetAxis("Horizontal") * speed * -1);
            if (Input.GetKey(KeyCode.Space))
                pos.y += 1 * speed;
            if (Input.GetKey(KeyCode.LeftShift))
                pos.y -= 1 * speed;
            transform.position = pos;
        }
    }
}
