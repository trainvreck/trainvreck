﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(SteamVR_LoadLevel))]
public class LevelManager : MonoBehaviour
{
    private SteamVR_LoadLevel steamVRLoadLevel;
    void Start()
    {
        steamVRLoadLevel = GetComponent<SteamVR_LoadLevel>();
        DontDestroyOnLoad(gameObject);
    }

    public void LoadIntro(float after = 0f)
    {
        StartCoroutine(LoadLevel("IntroScene", after));
    }

    public void LoadGame(float after = 0f)
    {
        StartCoroutine(LoadLevel("RaceScene", after));
    }

    private IEnumerator LoadLevel(string name, float time)
    {
        yield return new WaitForSeconds(time);
        steamVRLoadLevel.levelName = name;
        steamVRLoadLevel.Trigger();
    }
}
