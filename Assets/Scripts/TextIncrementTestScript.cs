﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextIncrementTestScript : MonoBehaviour
{
    TextMesh text;
    public 
        LeverController controller;
    void Start()
    {
        text = GetComponent<TextMesh>();
        controller.onValueChanged.AddListener(ChangeText);
    }

    private void ChangeText(float arg0)
    {
        text.text = arg0.ToString();
    }
}
